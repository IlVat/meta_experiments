import scala.collection.immutable.Seq
import scala.language.postfixOps
import scala.meta._

class playJsonReaderWriter extends scala.annotation.StaticAnnotation {
  inline def apply(defn: Any): Any = meta {
    
    import Helper._

    defn match {
      case Term.Block(Seq(t @ Trait(name, stats), companion: Defn.Object)) =>
        val oldCompObjTemplStat: Seq[Stat] = companion.templ.stats.getOrElse(Nil)
        val `trait` = (name, stats)
        val implicitObjects = generateImplObjects(`trait`, oldCompObjTemplStat)
        val newStats = implicitObjects.stats ++ oldCompObjTemplStat
        val newCompanion = companion.copy(templ = companion.templ.copy(stats = Some(newStats)))
        Term.Block(Seq(t, newCompanion))
      case Term.Block(Seq(Defn.Trait(mods, _, _, _, _), _)) if !Helper.isSealed(mods) => abort("trait must be sealed")
      case _ => abort("must be trait with two defs")
    }
  }

}

object Helper {

  type Trait = (Type.Name, Seq[Stat])

  val writes = "Writes"
  val reads = "Reads"

  def generateImplObjects(`trait`: Trait, stats: Seq[Stat]): Term.Block = {
    val ValidationErrorWrites = `object`(writes, `trait`._1, function(writes, `trait`, stats))
    val ValidationErrorReads = `object`(reads, `trait`._1, function(reads, `trait`, stats))

    q"""
       import play.api.libs.json._

       $ValidationErrorWrites
       $ValidationErrorReads"""
  }

  def `object`(name: String, tname: Type.Name, func: Defn.Def): Defn.Object = {
    q"implicit object ${Term.Name(tname + name) } extends ${ Ctor.Ref.Name(name) }[$tname] {$func}"
  }

  def function(objName: String, `trait`: Trait, stats: Seq[Stat]): Defn.Def = {
    val tname = `trait`._1
    val tstats = `trait`._2

    objName match {
      case name: String if name.toLowerCase == writes.toLowerCase => funcWrite(tname, tstats)
      case name: String if name.toLowerCase == reads.toLowerCase => funcRead(tname, tstats, stats)
    }
  }

  def funcWrite(tname: Type.Name, tstats: Seq[Stat]): Defn.Def = {
    val key = extractDefKey(tstats).getOrElse((Term.Name(""), Lit.String("")))._1
    val valueKey = extractDefKeyValueName(tstats).getOrElse(Term.Name(""))

    q"""override def writes(o: $tname): JsValue = {
      JsObject(Seq(o.$key -> JsString(o.$valueKey)))
    }"""
  }

  def funcRead(tname: Type.Name, tstats: Seq[Stat], stats: Seq[Stat]): Defn.Def = {
    val _expr = expr(stats, tstats)
    q"""override def reads(json: JsValue): JsResult[$tname] = { ${ _expr } }"""
  }

  def expr(cs: Seq[Stat], ts: Seq[Stat]): Term.Apply = {
    val defKeyValue: Lit.String = extractDefKey(ts).getOrElse((Term.Name(""), Lit.String("")))._2
    val pf = partialFunc(cs, ts)
    q"""(json \ $defKeyValue).validate[String].map{ $pf }"""
  }

  def partialFunc(cs: Seq[Stat], ts: Seq[Stat]): Term.PartialFunction = {
    val objNames = zipObjNames(zipParentChildrenNames(cs))
    val fieldName = extractDefKeyValueName(ts).getOrElse(Term.Name(""))
    val cases = buildMatcher(objNames, fieldName)
    q"""{..case $cases }"""
  }

  def extractDefKeyValueName(stats: Seq[Stat]): Option[Term.Name] = {
    stats.collect { case t: Decl.Def => t.name } headOption
  }

  def extractDefKey(stats: Seq[Stat]): Option[(Term.Name, Lit.String)] = {
    def extractValue(n: Defn.Def) = n.body match {
      case n: Lit.String => n
      case _ => Lit.String("")
    }

    stats.collect { case n: Defn.Def => (n.name, extractValue(n)) } headOption
  }

  def zipParentChildrenNames(st: Seq[Stat]): Seq[(Term.Name, Seq[Term.Name])] = {

    def children(obj: Defn.Object) = obj.templ.stats.getOrElse(Nil) match {
      case x @ ((_: Stat) :: _) => x.collect {
        case cobj: Defn.Object if isCase(cobj.mods) => cobj.name
      }
      case _ => Seq.empty[Term.Name]
    }

    st collect {
      case obj: Defn.Object =>
        val parent = obj.name
        (parent, children(obj))
    }
  }

  def zipObjNames(tn: Seq[(Term.Name, Seq[Term.Name])]): Seq[(Term.Name, Term.Name)] = {
    for {
      x <- tn if x._1.value != ""
      y <- x._2
    } yield (x._1, y)
  }

  def buildMatcher(objNames: Seq[(Term.Name, Term.Name)], field: Term.Name): Seq[Case] = objNames.map {
    name =>
      val obj = q"${ name._1 }.${ name._2 }"
      val selection = q"$obj.$field"
      p"case $selection => $obj"
  }

  def isSealed(mods: Seq[Mod]): Boolean = mods.exists(_.syntax == "sealed")

  def isCase(mods: Seq[Mod]): Boolean = mods.exists(_.syntax == "case")
}

object Trait {
  def unapply(any: Defn): Option[(Type.Name, Seq[Stat])] = any match {
    case t @ Defn.Trait(mod, name, _, _, Template(_, _, _, Some(Seq(Defn.Def(_, key, _, _, lit, _), Decl.Def(_, kv, _, _, _)))))
      if Helper.isSealed(mod) =>
      Some((t.name, t.templ.stats.getOrElse(Nil)))
    case _ => None
  }
}



