
@playJsonReaderWriter
sealed trait Greetings {
  def key: String = "greeting"
  def valueKey: String
}

object Greetings {

  sealed trait Arrival extends Greetings

  object Arrival {
    case object Hi extends Arrival {
      override val valueKey = "hi"
    }
    case object Hello extends Arrival {
      override val valueKey = "hello"
    }
  }

  sealed trait Leave extends Greetings

  object Leave {
    case object Bye extends Leave {
      override val valueKey = "bye"
    }
    case object Goodbye extends Leave {
      override val valueKey = "goodbye"
    }
  }
}






