import scala.collection.immutable.Seq
import scala.meta._

/**
  * Created by ivatolin on 04.07.2017.
  */


/*
* Don't use case classes inside companion object!!! It doesn't work yet.
*
* Before:
* sealed trait CustomError extends ValidationError
*
* object CustomError {
*   case object NotFound extends CustomError
*   case object Exists extends CustomError
*   case object CustomDbError extends DBError with CustomError {
*     override val valueKey = "custom.error" -- if object extends another entity where keyValue has already defined
*                                            -- you need override the val because annotation do analyses syntax tree only
*                                            -- in code below it in the same file
*   }
* }
*
* After:
* @ValidationErrorReadsWrites
* sealed trait CustomError extends ValidationError
* object CustomError {
*   case object NotFound extends CustomError
*   case object Exists extends CustomError
*   case object CustomDbError extends DBError with CustomError {
*     override val valueKey = "custom.error"
*   }
*
*   implicit object CustomErrorWrites extends Writes[CustomError] {
*      override def writes(o: CustomError): JsValue = {
*         JsObject(Seq(o.key -> JsString(o.valueKey)))
*       }
*   }
*
*   implicit object CustomErrorReads extends Reads[CustomError] {*
*       override def reads(json: JsValue): JsResult[CustomError] = {
*         (json \ "error").validate[String].map {
*           case NotFound.valueKey => NotFound
*           case Exists.valueKey => Exists
*           case CustomDbError.valueKey => CustomDbError
*         }
*       }
*    }
* }
*
* */

//:TODO smarter mechanism of handling unacceptable cases
class ValidationErrorReadsWrites extends scala.annotation.StaticAnnotation {
  inline def apply(defn: Any): Any = meta {

    import MacroHelper._

    val (tr, companion) = defn match {
      case q"${tr: Defn.Trait}; ${obj: Defn.Object}" if isSealed(tr.mods) => (tr, obj)
      case q"${tr: Defn.Trait}" => abort("@ValidationError must have companion object")
      case _ => abort("@ValidationError works on sealed traits")
    }

    val newStats = generateImplObjects(companion.name, companion.templ.stats.getOrElse(Nil))

    val newCompanion = companion.copy(
      templ = companion.templ.copy(
        stats = Some(companion.templ.stats.getOrElse(Nil) ++ newStats.stats)
      )
    )

    q"$tr; $newCompanion"
  }
}

object MacroHelper {

  val writes = "Writes"
  val reads = "Reads"

  def generateImplObjects(objName: Term.Name, stats: Seq[Stat]): Term.Block = {
    val ErrorWrites = `object`(writes, objName, function(writes, objName, stats))
    val ErrorReads = `object`(reads, objName, function(reads, objName, stats))

    q"""
       import play.api.libs.json._

       $ErrorWrites
       $ErrorReads"""
  }

  def `object`(name: String, objName: Term.Name, func: Defn.Def): Defn.Object = {
    q"implicit object ${ Term.Name(objName.value + name) } extends ${ Ctor.Ref.Name(name) }[${ Type.Name(objName.value) }] {$func}"
  }

  def function(fname: String, objName: Term.Name, stats: Seq[Stat]): Defn.Def = {
    objName match {
      case name: Term.Name if fname.toLowerCase == writes.toLowerCase => funcWrite(name)
      case name: Term.Name if fname.toLowerCase == reads.toLowerCase => funcRead(name, stats)
    }
  }

  //:TODO flexible mechanism of detecting fields in the trait ValidationError
  def funcWrite(objName: Term.Name): Defn.Def = {
    q"""override def writes(o: ${ Type.Name(objName.value) }): JsValue = {
      JsObject(Seq(${ Lit.String("error") } -> JsString(o.${ Term.Name("valueKey") })))
    }"""
  }

  def funcRead(objName: Term.Name, stats: Seq[Stat]): Defn.Def = {
    val _expr = expr(stats)
    q"""override def reads(json: JsValue): JsResult[${ Type.Name(objName.value) }] = { ${ _expr } }"""
  }

  def expr(stats: Seq[Stat]): Term.Apply = {
    val pf = partialFunc(stats)
    q"""(json \ ${ Lit.String("error") }).validate[String].map{ $pf }"""
  }

  def partialFunc(stats: Seq[Stat]): Term.PartialFunction = {
    val objAndVal = zipObjAndVal(stats)
    val cases = buildMatcher(objAndVal)
    q"""{..case $cases }"""
  }

  def buildMatcher(params: Seq[(Term.Name, Term.Name)]): Seq[Case] = params.map {
    case (objName, keyValue) => p"case $objName.$keyValue => $objName"
  }


  def zipObjAndVal(st: Seq[Stat]): Seq[(Term.Name, Term.Name)] = {

    def valParam[T](obj: Defn.Object, f: Defn.Val => T): Option[T] = obj.templ.stats.getOrElse(Nil) match {
      case x @ ((_: Stat) :: _) => x.collect { case _val: Defn.Val => f(_val) } headOption
      case _ => None
    }

    def valKey(_val: Defn.Val): Term.Name = {
      val key = _val.pats.collect { case key: Pat.Var.Term => key.name }.headOption
      key.getOrElse(Term.Name(""))
    }

    st collect {
      case obj: Defn.Object =>
        val objName = obj.name
        val _valKey = valParam[Term.Name](obj, valKey).getOrElse(Term.Name(""))
        (objName, _valKey)
    }
  }

  def isSealed(mods: Seq[Mod]): Boolean = mods.exists(_.syntax == "sealed")

  def isCase(mods: Seq[Mod]): Boolean = mods.exists(_.syntax == "case")
}

