
import org.scalatest.{FunSuite, Matchers}



class ValidationErrorReadsWritesTest extends FunSuite with Matchers {
  test("Generation of implicit readers and writers") {
    """
    import play.api.libs.json._

    import UserError.UserErrorReads._
    import UserError.UserErrorWrites._

    import UserError._

    Json.fromJson[UserError](Json.parse("{\"error\":\"error.user.not_found\"}"))

    Json.toJson(UserNotFound)

  """ should compile
  }
}

trait ValidationError {
  def valueKey: String
}

@ValidationErrorReadsWrites
sealed trait UserError extends ValidationError

trait DbError {
  val valueKey = "error"
}

object UserError {

  case object UserNotFound extends UserError { val valueKey = "error.user.not_found" }

  case object UserExists extends UserError { val valueKey = "error.user.exists" }

  case object DbError extends DbError with UserError {
    override val valueKey = "db.error"
  }
}


