name := "meta_experiments"

lazy val ParadiseVersion = "3.0.0-M9"
lazy val paradise = "org.scalameta" % "paradise" % ParadiseVersion cross CrossVersion.full
lazy val scalameta = "org.scalameta" %% "scalameta" % "1.8.0"
lazy val akkaHttpPlayJson = "de.heikoseeberger" %% "akka-http-play-json" % "1.15.0"
lazy val cats = "org.typelevel" %% "cats" % "0.9.0"
lazy val scalatest = "org.scalatest" %% "scalatest" % "3.0.1" % Test
lazy val testkit = "org.scalameta" %% "testkit" % "1.8.0"

lazy val allSettings = Seq(
  organization := "org.test",
  scalaVersion := "2.11.11",
  resolvers += Resolver.bintrayRepo("scalameta", "maven"),
  updateOptions := updateOptions.value.withCachedResolution(true)
)


lazy val macroSettings = Seq(
  libraryDependencies ++= Seq(scalameta, akkaHttpPlayJson, cats, scalatest, testkit),
  addCompilerPlugin(paradise),
  scalacOptions += "-Xplugin-require:macroparadise"
)

lazy val root = (project in file("./"))
.settings(allSettings, macroSettings)